# rust-ebpf

A helloworld sample using Rust and eBPF to demonstrate dynamic tracing an application.

It uses the [libprobe](https://github.com/cuviper/rust-libprobe) `probe!()` macro to fire a simple USDT in a loop every 3 seconds.

You can then use probe.py to enable the trace points and listen for events.

### prereqs

* Install Rust Nightly `$ curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain nightly`

* [Install the bcc-tools](https://github.com/iovisor/bcc/blob/master/INSTALL.md)


### usage

```
$ cargo run
```

In a new console run

```
$ ps aux | grep rust-ebpf
anton     5478  0.0  0.0   2800   792 pts/0    S+   21:36   0:00 target/debug/rust-ebpf
...

$ sudo ./probe.py 5478
TIME(s)            COMM             PID    MESSAGE
9129.761490000     rust-ebpf        5478   Hello, World!
9132.762761000     rust-ebpf        5478   Hello, World!
9135.765193000     rust-ebpf        5478   Hello, World!
```