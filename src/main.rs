#![feature(asm)]
#[macro_use] #[no_link]

extern crate probe;

use std::{thread, time};
use std::i32::MAX;

fn main() {

    println!("{}", "Running rust-ebpf");
    let mut number = 0;
    loop {
        
        let secs = time::Duration::from_millis(3000);
        thread::sleep(secs);
        if number + 1 >= MAX {
            number = 0;
        }
        number += 1;
        probe!(foo, loop);
    }
}
